﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameConsoleDB.Models.Entities
{
    public class Manufacturer
    {
        public int Id { get; set; }
        [DisplayName("Manufacturer"), DefaultValue("N/A"), Required]
        public string name { get; set; }
        [DisplayName("Date Founded"), DefaultValue("01")]
        public DateTime dateFounded { get; set; }
        [DisplayName("Date Defunct")]
        public DateTime dateDefunct { get; set; }
        [DisplayName("Still Releasing"), DefaultValue(true), Required]
        public Boolean stillReleasing { get; set; }

        public Manufacturer()
        {
            string minDate = "01/01/1900";
            DateTime minDateTime = Convert.ToDateTime(minDate);

            if(this.dateDefunct <= DateTime.Now)
            {
                this.stillReleasing = false;
            }

            if(this.dateFounded <= minDateTime)
            {
                this.dateFounded = minDateTime;
            }

            if (this.dateDefunct <= this.dateFounded)
            {
                this.dateDefunct = this.dateFounded;
            }
        }
    }
}
