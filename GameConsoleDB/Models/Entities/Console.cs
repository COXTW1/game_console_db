﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GameConsoleDB.Models.Entities
{
    public class Console
    {
        public int Id { get; set; }
        [DisplayName("Name"), DefaultValue("N/A"), Required]
        public string name { get; set; }
        [DisplayName("Alternative Names")]
        public List<string> altNames { get; set; }
        [DisplayName("Released"), Required]
        public DateTime releaseDate { get; set; }
        [DisplayName("Discontinued")]
        public DateTime discontinuationDate {get; set;}
        [DisplayName("Region Locked"), DefaultValue(false)]
        public Boolean regionLocked { get; set; }
<<<<<<< HEAD
=======

        [DisplayName("Party"), DefaultValue("Unknown")]
        public string party { get; set; } 
        [DisplayName("Number of Add-Ons"), DefaultValue(0)]
        public int numAddons { get; set; }
        [DisplayName("Number of Controllers"), DefaultValue(0)]
        public int numControllers { get; set; }
        [DisplayName("number of Save Media Types"), DefaultValue(0)]
        public int numSaveMedia { get; set; }
        [DisplayName("Console Manufacturer")]
        public Manufacturer manufacturer { get; set; }
          

        public Console()
        {
            string minDate = "01/01/1900";

            this.manufacturer = new Manufacturer
            {
                Id = 0,
                name = "N/A",
                dateFounded = Convert.ToDateTime(minDate),
                dateDefunct = Convert.ToDateTime(minDate),
                stillReleasing = false
            };
        }
    }
}
